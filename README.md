dotfiles.git
============

```sh
cd $HOME
git clone https://bitbucket.org/10labs_team/dotfiles.git 
ln -sb dotfiles/.bash_profile .
ln -sb dotfiles/.bashrc .
ln -sb dotfiles/.bashrc_custom .
```
